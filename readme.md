
# Adding Docker Container Data Persistance
In this project we will cover how to persist our data from Docker containers.

## Technologies Used
- Docker
- Amazon ECR
- Node.js
- MongoDB
- Mongo-Express
- Git
- Linux (Ubuntu)

## Project Description
- Persist data of a MongoDB container by attaching a Docker volume to it

## Prerequisites
- A Node.js application built and uploaded to our private Amazon ECR
- Able to run our `docker compose -f mongo-no-persistance.yaml up` and have your Node.js app, MongoDB, and Mongo-Express containers all start up successfully.

## Guide Steps
- Run `docker compose -f mongo-no-persistance.yaml up` and we can see that the 3 containers start and via Mongo-Express there is a default empty state without our extra database and collection that we need.

![Default Database State](/images/m7-5-database-default-state.png)

- If we were to stop the containers at this point, any changes we make the the database will be lost. We will add the volumes field for our docker compose yaml to persist the data.
- Recreate our containers
	- `docker compose -f mongo-persistance.yaml up`
- Make a change to the data and verify the data is in our Database via Mongo-Express
- Stop the containers
	- `docker compose -f mongo-persistance.yaml down`
- Start the containers to verify data persists
	- `docker compose -f mongo-persistance.yaml up`

![Data Was Saved](/images/m7-5-data-persists.png)
